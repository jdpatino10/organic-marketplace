/**
 * Created by IvanGarcia on 13/05/2016.
 */
(function (ng) {
    var mod = ng.module('loginConsumidorModule');

    mod.controller('loginConsumidorCtrl', ['$scope', 'loginConsumidorService', '$window', '$location', function ($scope, loginConsumidorService, $window, $location) {
        function responseError(response) {
            console.log(response);
        }
  this.logIn = function () {
            if ($scope.userForm.username == undefined || $scope.userForm.password == undefined) {
                var error = '{"mensaje": "error"}';
                $scope.errorLogin = true;
                return error;
            }
            return loginConsumidorService.logIn($scope.userForm.username, $scope.userForm.password).then(function (response) {
                if (response.data.status != 'ERROR') {
                    window.location = '/productos';
                } else {
                    $scope.userProvider = response.data;
                    $scope.errorLogin = true;
                }
            }, responseError);
        };

    }]);
})(window.angular);