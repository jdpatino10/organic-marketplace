/**
 * Created by Juan on 17/04/16.
 */
(function (ng) {
    var mod = ng.module('loginConsumidorModule');

    mod.service('loginConsumidorService', ['$http', 'loginConsumidorContext', function ($http, context) {

  this.logIn = function (username, password) {
            return $http({
                method: 'POST',
                url: '/movil/login/',
                data: {
                    username: username,
                    password: password
                }
            });
        };

    }]);
})(window.angular);