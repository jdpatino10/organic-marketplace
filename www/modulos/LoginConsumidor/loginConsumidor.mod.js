/**
 * Created by IvanGarcia on 13/05/2016.
 */
(function (ng) {
    var mod = ng.module('loginConsumidorModule', ['ui.bootstrap']);

    mod.constant('loginConsumidorContext', 'loginConsumidor');

})(window.angular)