/**
 * Created by IvanGarcia on 04/05/2016.
 */
(function (ng) {
    var mod = ng.module('comprasProductorModule', ['ui.bootstrap']);

    mod.constant('comprasProductorContext', 'comprasProductor');

})(window.angular)