/**
 * Created by IvanGarcia on 04/05/2016.
 */
(function (ng) {
    var mod = ng.module('comprasProductorModule');

    mod.controller('comprasProductorCtrl', ['$scope', 'comprasProductorService', '$window', function ($scope,comprasProductorService, $window) {
        function responseError(response) {
            console.log(response);
        }
         this.getOffers = function () {
            return comprasProductorService.getOffers().then(function (response) {
                $scope.offers = response.data;
            }, responseError);
        };
   $scope.goToPurchaseDetail = function (purchaseId) {
            localStorage.setItem("purchaseId", purchaseId);
            $location.path('/compra_productos');
        };

    }]);



})(window.angular);
