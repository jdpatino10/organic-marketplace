/**
 * Created by IvanGarcia on 04/05/2016.
 */
(function (ng) {
    var mod = ng.module('comprasProductorModule');

    mod.service('comprasProductorService', ['$http', 'comprasProductorContext', function ($http, context) {

          this.getOffers = function () {
                return $http({
                    method: 'GET',
                    url: '/compra/comprasProductor/'
                });
    };
    }]);
})(window.angular);