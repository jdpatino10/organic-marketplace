/**
 * Created by IvanGarcia on 12/03/2016.
 */
(function (ng) {
    var mod = ng.module('productoModule');

    mod.controller('productoCtrl', ['$scope', 'productoService', '$window', function ($scope,productoService, $window) {
        $scope.filteredTodos = [];
        $scope.productosSemana = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 8;
        $scope.totalItems = 0;
        $scope.maxSize = 5;
        $scope.$watch('currentPage + numPerPage', function () {

            var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin + $scope.numPerPage;
            $scope.filteredTodos = $scope.productosSemana.slice(begin, end);

        });

        function responseError(response) {
            console.log(response);
        }
        this.getOffers = function () {
            return productoService.getOffers().then(function (response) {
                $scope.offers = response.data;
            }, responseError);
        };
         this.deleteProduct = function (pk) {
            return productoService.deleteProduct(pk).then(function (response) {
                window.location.reload(true);
            }, responseError);
        };

        this.productosSemana = function () {
            return productoService.productosSemana().then(function (response) {
                $scope.productosSemana = response.data;
                var numPaginas =$scope.productosSemana.length%8;
                if (numPaginas==0){
                   $scope.totalItems = $scope.productosSemana.length;
                }else{
                 $scope.totalItems = Math.ceil($scope.productosSemana.length/8)*8;
                }
                $scope.filteredTodos = $scope.productosSemana.slice(0, 8)

            }, responseError);
        };
    }]);


})(window.angular);
