# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Compra', '0002_compra_productor'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compra',
            name='productor',
        ),
    ]
