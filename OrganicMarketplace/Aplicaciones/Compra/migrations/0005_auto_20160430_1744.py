# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Productor', '0002_auto_20160308_2217'),
        ('Compra', '0004_compra_productor'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compra',
            name='productor',
        ),
        migrations.AddField(
            model_name='compra_producto',
            name='productor',
            field=models.ForeignKey(to='Productor.Productor', null=True),
        ),
    ]
