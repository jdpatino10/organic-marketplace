import json

#from bson import json_util
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Compra, Compra_Producto, TiempoCarrito
from django.core import serializers


# Create your views here.
@csrf_exempt
def compras(request):
    if request.method == 'GET':
        compras = Compra.objects.all().values('pk', 'fecha', 'totalCompra', 'estado')

        for compra in compras:
            compra['fecha'] = str(compra['fecha'])

        compras = json.loads(json.dumps(list(compras)))
        return JsonResponse(compras, safe=False)


@csrf_exempt
def detalle_compra(request, pk):
    if request.method == 'GET':
        compra = Compra.objects.get(pk=pk)
        compra_productos = Compra_Producto.objects.filter(compra_id=compra.pk).values('producto_id__nombre',
                                                                                      'mercadoTipo_id__nombre',
                                                                                      'cantidad',
                                                                                      'producto_id__unidadMedida_id__abreviatura',
                                                                                      'valor')
        compra_productos = json.loads(json.dumps(list(compra_productos)))
        return JsonResponse(compra_productos, safe=False)

@csrf_exempt
def comprasProductor(request):
        current_user = request.user
        print current_user.id
        compras = Compra_Producto.objects.filter(productor__usuarioId_id=current_user.pk).values('pk', 'compra__fecha', 'compra__totalCompra', 'compra__estado','producto__nombre','compra__consumidor__direccion')
        for compra in compras:
            compra['compra__fecha'] = str(compra['compra__fecha'])
        compras = json.loads(json.dumps(list(compras)))
        return JsonResponse(compras, safe=False)


@csrf_exempt
def tiempoCarritoCompras(request):
    if request.method == 'GET':
        tiempoCarrito = TiempoCarrito.objects.all()
        return HttpResponse(serializers.serialize("json",tiempoCarrito))
    if request.method == 'POST':
        TiempoCarrito.objects.all().delete()
        jsonData = json.loads(request.body)
        tiempoCarrito = TiempoCarrito()
        tiempoCarrito.tiempo = jsonData['tiempo']
        tiempoCarrito.save()
        return HttpResponse(serializers.serialize("json", [tiempoCarrito]))